from PyQt5.QtWidgets import (
    QWidget,
    QLabel,
    QVBoxLayout,
    QHBoxLayout,
    QFormLayout,
    QGroupBox,
    QSlider,
    
)

from PyQt5.QtCore import Qt

from typing import Dict

from twinpy.twinpy.ui import TcMainWindow
from twinpy.twinpy.twincat import TwincatConnection, Symbol, Signal, Parameter
from twinpy.twinpy.ui import TcLineEdit, TcLabel


class MyPlcGUI(TcMainWindow):
    """GUI for demo PLC"""

    def __init__(self, plc: TwincatConnection):

        self.plc = plc

        super().__init__()  # Parent constructor

        main_widget = QWidget()  # Widget spanning the entire window
        self.layout_main = QVBoxLayout(main_widget)  # Main layout
        self.setCentralWidget(main_widget)  # Make widget the main thingy

        # In this dict we will store all symbol instances
        self.symbols: Dict[str, Symbol] = {

            #current timestamp
            'current_timeStamp':Signal(plc=self.plc, name='MAIN.my_struct_Cleg.currentTimeReceived'),

            #Cleg data
            'Cleg_knee_angle':Signal(plc=self.plc, name='MAIN.my_struct_Cleg.kneeAngle'),
            'my_time': Signal(plc=self.plc, name='MAIN.my_struct_Cleg.timeReceived'),
            'Cleg_knee_angle_velocity': Signal(plc=self.plc, name='MAIN.my_struct_Cleg.kneeAngleVelocity'),
            'Cleg_Roll_angle': Signal(plc=self.plc, name='MAIN.my_struct_Cleg.RollAngle'),
            'Cleg_Pitch_angle': Signal(plc=self.plc, name='MAIN.my_struct_Cleg.PitchAngle'),
            'Cleg_Yaw_angle': Signal(plc=self.plc, name='MAIN.my_struct_Cleg.YawAngle'),
            'Cleg_Roll_angle_velocity': Signal(plc=self.plc, name='MAIN.my_struct_Cleg.RollAngleVelocity'),
            'Cleg_Pitch_angle_velocity': Signal(plc=self.plc, name='MAIN.my_struct_Cleg.PitchAngleVelocity'),
            'Cleg_Yaw_angle_velocity': Signal(plc=self.plc, name='MAIN.my_struct_Cleg.YawAngleVelocity'),
            'Cleg_accx': Signal(plc=self.plc, name='MAIN.my_struct_Cleg.accx'),
            'Cleg_accy': Signal(plc=self.plc, name='MAIN.my_struct_Cleg.accy'),
            'Cleg_accz': Signal(plc=self.plc, name='MAIN.my_struct_Cleg.accz'),

             #empower data
            'Empower_ballscrew_pos_counts': Signal(plc=self.plc, name='MAIN.my_struct_emp.Empower_ballscrew_pos_counts'),
            'Empower_ballscrew_pos_counts_ts':Signal(plc=self.plc, name='MAIN.my_struct_emp.Empower_ballscrew_pos_counts_ts'),
            'Empower_ankle_angle':Signal(plc=self.plc, name='MAIN.my_struct_emp.Empower_ankle_angle'),
            'Empower_ankle_angle_ts': Signal(plc=self.plc, name='MAIN.my_struct_emp.Empower_ankle_angle_ts'),
            'Empower_series_spring_torque':Signal(plc=self.plc, name='MAIN.my_struct_emp.Empower_series_spring_torque'),
            'Empower_series_spring_torque_ts':Signal(plc=self.plc, name='MAIN.my_struct_emp.Empower_series_spring_torque_ts'),
            'Empower_hardstop_torque':Signal(plc=self.plc, name='MAIN.my_struct_emp.Empower_hardstop_torque'),
            'Empower_hardstop_torque_ts':Signal(plc=self.plc, name='MAIN.my_struct_emp.Empower_hardstop_torque_ts'),
            'Empower_input_current': Signal(plc=self.plc, name='MAIN.my_struct_emp.Empower_input_current'),
            'Empower_input_current_ts':Signal(plc=self.plc, name='MAIN.my_struct_emp.Empower_input_current_ts'),
            'Empower_motor_u_current':Signal(plc=self.plc, name='MAIN.my_struct_emp.Empower_motor_u_current'),
            'Empower_motor_u_current_ts':Signal(plc=self.plc, name='MAIN.my_struct_emp.Empower_motor_u_current_ts'),
            'Empower_motor_v_current':Signal(plc=self.plc, name='MAIN.my_struct_emp.Empower_motor_v_current'),
            'Empower_motor_v_current_ts':Signal(plc=self.plc, name='MAIN.my_struct_emp.Empower_motor_v_current_ts'),
            'Empower_bridge_temp':Signal(plc=self.plc, name='MAIN.my_struct_emp.Empower_bridge_temp'),
            'Empower_bridge_temp_ts':Signal(plc=self.plc, name='MAIN.my_struct_emp.Empower_bridge_temp_ts'),
            'Empower_motor_temp':Signal(plc=self.plc, name='MAIN.my_struct_emp.Empower_motor_temp'),
            'Empower_motor_temp_ts':Signal(plc=self.plc, name='MAIN.my_struct_emp.Empower_motor_temp_ts'),

             'Empower_accel_x':Signal(plc=self.plc, name='MAIN.my_struct_emp.Empower_accel_x'),
             'Empower_accel_x_ts':Signal(plc=self.plc, name='MAIN.my_struct_emp.Empower_accel_x_ts'),
             'Empower_accel_y':Signal(plc=self.plc, name='MAIN.my_struct_emp.Empower_accel_y'),
             'Empower_accel_y_ts':Signal(plc=self.plc, name='MAIN.my_struct_emp.Empower_accel_y_ts'),
             'Empower_accel_z':Signal(plc=self.plc, name='MAIN.my_struct_emp.Empower_accel_z'),
             'Empower_accel_z_ts':Signal(plc=self.plc, name='MAIN.my_struct_emp.Empower_accel_z_ts'),
             'Empower_gyro_x':Signal(plc=self.plc, name='MAIN.my_struct_emp.Empower_gyro_x'),
             'Empower_gyro_x_ts': Signal(plc=self.plc, name='MAIN.my_struct_emp.Empower_gyro_x_ts'),
             'Empower_gyro_y':Signal(plc=self.plc, name='MAIN.my_struct_emp.Empower_gyro_y'),
             'Empower_gyro_y_ts':Signal(plc=self.plc, name='MAIN.my_struct_emp.Empower_gyro_y_ts'),
             'Empower_gyro_z':Signal(plc=self.plc, name='MAIN.my_struct_emp.Empower_gyro_z'),
             'Empower_gyro_z_ts':Signal(plc=self.plc, name='MAIN.my_struct_emp.Empower_gyro_z_ts'),

             'Empower_run_level':Signal(plc=self.plc, name='MAIN.my_struct_emp.Empower_run_level'),
             'Empower_run_level_ts':Signal(plc=self.plc, name='MAIN.my_struct_emp.Empower_run_level_ts'),

             #output
             'output_Empower_run_level': Parameter(plc=self.plc, name='MAIN.out_struct.Empower_run_level'),
             
             'output_Empower_current_sp': Parameter(plc=self.plc, name='MAIN.out_struct.Empower_current_sp')
             
            
            # 'increment': Parameter(plc=self.plc, name='MAIN.increment'),
            # 'my_double': Parameter(plc=self.plc, name='MAIN.my_double'),
        }

        # ------- Input boxes -------

        # self.input = TcLineEdit(
        #     symbol=self.symbols['output'],
        #     #format='%d'
        # )
        #self.input_double = TcLineEdit(symbol=self.symbols['my_double'])

        # group_input = QGroupBox("Input")
        # layout_group_input = QFormLayout(group_input)  # Create layout for group

        # #Now add widgets to layout:
        # layout_group_input.addRow(QLabel("Increment"), self.input_increment)
        # layout_group_input.addRow(QLabel("MyDouble"), self.input_double)

        # self.layout_main.addWidget(group_input)

        # ------- Labels -------

        #current timeStamp
        self.label_time = TcLabel(
            symbol=self.symbols['current_timeStamp'],
            #format='%d'
        )

        #Cleg Inputs

        self.label_angle = TcLabel(
            symbol=self.symbols['Cleg_knee_angle'],
            #format='%d'
        )

        self.label_timestamp = TcLabel(
            symbol=self.symbols['my_time']
        )

        self.label_angleVel = TcLabel(
            symbol=self.symbols['Cleg_knee_angle_velocity']
        )

        self.label_roll = TcLabel(
            symbol=self.symbols['Cleg_Roll_angle']
        )

        self.label_pitch = TcLabel(
            symbol=self.symbols['Cleg_Pitch_angle']
        )

        self.label_yaw = TcLabel(
            symbol=self.symbols['Cleg_Yaw_angle']
        )

        self.label_rollVel = TcLabel(
            symbol=self.symbols['Cleg_Roll_angle_velocity']
        )

        self.label_pitchVel = TcLabel(
            symbol=self.symbols['Cleg_Pitch_angle_velocity']
        )

        self.label_yawVel = TcLabel(
            symbol=self.symbols['Cleg_Yaw_angle_velocity']
        )

        self.label_accx = TcLabel(
            symbol=self.symbols['Cleg_accx']
        )

        self.label_accy = TcLabel(
            symbol=self.symbols['Cleg_accy']
        )
       
        self.label_accz = TcLabel(
            symbol=self.symbols['Cleg_accz']
        )

        #empower inputs 


        self.label_Empower_ballscrew_pos_counts = TcLabel(
            symbol=self.symbols['Empower_ballscrew_pos_counts']
        )

        self.label_Empower_ballscrew_pos_counts_ts= TcLabel(
            symbol=self.symbols['Empower_ballscrew_pos_counts_ts']
        )

        self.label_Empower_ankle_angle= TcLabel(
            symbol=self.symbols['Empower_ankle_angle']
        )

        self.label_Empower_ankle_angle_ts= TcLabel(
            symbol=self.symbols['Empower_ankle_angle_ts']
        )

        self.label_Empower_series_spring_torque= TcLabel(
            symbol=self.symbols['Empower_series_spring_torque']
        )

        self.label_Empower_series_spring_torque_ts= TcLabel(
            symbol=self.symbols['Empower_series_spring_torque_ts']
        )

        self.label_Empower_hardstop_torque= TcLabel(
            symbol=self.symbols['Empower_hardstop_torque']
        )

        self.label_Empower_hardstop_torque_ts= TcLabel(
            symbol=self.symbols['Empower_hardstop_torque_ts']
        )

        self.label_Empower_input_current= TcLabel(
            symbol=self.symbols['Empower_input_current']
        )

        self.label_Empower_input_current_ts= TcLabel(
            symbol=self.symbols['Empower_input_current_ts']
        )

        self.label_Empower_motor_u_current= TcLabel(
            symbol=self.symbols['Empower_motor_u_current']
        )

        self.label_Empower_motor_u_current_ts= TcLabel(
            symbol=self.symbols['Empower_motor_u_current_ts']
        )

        self.label_Empower_motor_v_current= TcLabel(
            symbol=self.symbols['Empower_motor_v_current']
        )

        self.label_Empower_motor_v_current_ts= TcLabel(
            symbol=self.symbols['Empower_motor_v_current_ts']
        )

        self.label_Empower_bridge_temp= TcLabel(
            symbol=self.symbols['Empower_bridge_temp']
        )

        self.label_Empower_bridge_temp_ts= TcLabel(
            symbol=self.symbols['Empower_bridge_temp_ts']
        )

        self.label_Empower_motor_temp= TcLabel(
            symbol=self.symbols['Empower_motor_temp']
        )

        self.label_Empower_motor_temp_ts= TcLabel(
            symbol=self.symbols['Empower_motor_temp_ts']
        )

        self.label_Empower_accel_x= TcLabel(
            symbol=self.symbols['Empower_accel_x']
        )

        self.label_Empower_accel_x_ts= TcLabel(
            symbol=self.symbols['Empower_accel_x_ts']
        )

        self.label_Empower_accel_y= TcLabel(
            symbol=self.symbols['Empower_accel_y']
        )

        self.label_Empower_accel_y_ts= TcLabel(
            symbol=self.symbols['Empower_accel_y_ts']
        )

        self.label_Empower_accel_z= TcLabel(
            symbol=self.symbols['Empower_accel_z']
        )
       

        self.label_Empower_accel_z_ts= TcLabel(
            symbol=self.symbols['Empower_accel_z_ts']
        )

        self.label_Empower_gyro_x= TcLabel(
            symbol=self.symbols['Empower_gyro_x']
        )

        self.label_Empower_gyro_x_ts= TcLabel(
            symbol=self.symbols['Empower_gyro_x_ts']
        )

        self.label_Empower_gyro_y= TcLabel(
            symbol=self.symbols['Empower_gyro_y']
        )

        self.label_Empower_gyro_y_ts= TcLabel(
            symbol=self.symbols['Empower_gyro_y_ts']
        )

        self.label_Empower_gyro_z= TcLabel(
            symbol=self.symbols['Empower_gyro_z']
        )

        self.label_Empower_gyro_z_ts= TcLabel(
            symbol=self.symbols['Empower_gyro_z_ts']
        )

        self.label_Empower_run_level= TcLabel(
            symbol=self.symbols['Empower_run_level']
        )

        self.label_Empower_run_level_ts= TcLabel(
            symbol=self.symbols['Empower_run_level_ts']
        )

        #output label

        self.input_current = TcLineEdit(
            symbol=self.symbols['output_Empower_current_sp'], 
             format='%f'                   
        )

        self.input_runlevel = TcLineEdit(
            symbol=self.symbols['output_Empower_run_level'],
            format='%d'
            
        )

        group_output = QGroupBox("Output")
        layout_group_output = QFormLayout(group_output)  # Create layout for group

        group_input = QGroupBox("Input")
        layout_group_input = QFormLayout(group_input)  # Create layout for second group

        # Now add widgets to layout:
        layout_group_output.addRow(QLabel("current_time"), self.label_time)

        layout_group_output.addRow(QLabel("Cleg_knee_angle"), self.label_angle)
        layout_group_output.addRow(QLabel("my_time"), self.label_timestamp)
        layout_group_output.addRow(QLabel("Cleg_knee_angle_velocity"), self.label_angleVel)
        layout_group_output.addRow(QLabel("Cleg_Roll_angle"), self.label_roll)
        layout_group_output.addRow(QLabel("Cleg_Pitch_angle"), self.label_pitch)
        layout_group_output.addRow(QLabel("Cleg_Yaw_angle"), self.label_yaw)
        layout_group_output.addRow(QLabel("Cleg_Roll_angle_velocity"), self.label_rollVel)
        layout_group_output.addRow(QLabel("Cleg_Pitch_angle_velocity"), self.label_pitchVel)
        layout_group_output.addRow(QLabel("Cleg_Yaw_angle_velocity"), self.label_yawVel)
        layout_group_output.addRow(QLabel("Cleg_accx"), self.label_accx)
        layout_group_output.addRow(QLabel("Cleg_accy"), self.label_accy)
        layout_group_output.addRow(QLabel("Cleg_accz"), self.label_accz)

        layout_group_output.addRow(QLabel("Empower_run_level_ts"), self.label_Empower_run_level_ts)
        layout_group_output.addRow(QLabel("Empower_run_level"), self.label_Empower_run_level)
        layout_group_output.addRow(QLabel("Empower_gyro_z_ts"), self.label_Empower_gyro_z_ts)
        layout_group_output.addRow(QLabel("Empower_gyro_z"), self.label_Empower_gyro_z)

        layout_group_output.addRow(QLabel("Empower_gyro_y_ts"), self.label_Empower_gyro_y_ts)
        layout_group_output.addRow(QLabel("Empower_gyro_y"), self.label_Empower_gyro_y)

        layout_group_output.addRow(QLabel("Empower_gyro_x_ts"), self.label_Empower_gyro_x_ts)
        layout_group_output.addRow(QLabel("Empower_gyro_x"), self.label_Empower_gyro_x)

        layout_group_output.addRow(QLabel("Empower_accel_z_ts"), self.label_Empower_accel_z_ts)
        layout_group_output.addRow(QLabel("Empower_accel_z"), self.label_Empower_accel_z)

        layout_group_output.addRow(QLabel(" Empower_accel_y_ts"), self.label_Empower_accel_y_ts)
        layout_group_output.addRow(QLabel(" Empower_accel_y_ts"), self.label_Empower_accel_y)

        layout_group_output.addRow(QLabel("Empower_accel_x_ts"), self.label_Empower_accel_x_ts)
        layout_group_output.addRow(QLabel("Empower_accel_x"), self.label_Empower_accel_x)

        
        layout_group_output.addRow(QLabel("Empower_motor_temp_ts"), self.label_Empower_motor_temp_ts)
        layout_group_output.addRow(QLabel("Empower_motor_temp"), self.label_Empower_motor_temp)
        layout_group_output.addRow(QLabel("Empower_bridge_temp"), self.label_Empower_bridge_temp)        
        layout_group_output.addRow(QLabel("Empower_bridge_temp_ts"), self.label_Empower_bridge_temp_ts)

        layout_group_output.addRow(QLabel("Empower_motor_v_current_ts"), self.label_Empower_motor_v_current_ts)
        layout_group_output.addRow(QLabel("Empower_motor_v_current"), self.label_Empower_motor_v_current)


        layout_group_output.addRow(QLabel("Empower_motor_u_current_ts"), self.label_Empower_motor_u_current_ts)
        layout_group_output.addRow(QLabel("Empower_motor_u_current"), self.label_Empower_motor_u_current)

        # layout_group_output.addRow(QLabel("Empower_input_current_ts"), self.label_Empower_input_current_ts)        
        # layout_group_output.addRow(QLabel("Empower_input_current"), self.label_Empower_input_current)

        layout_group_output.addRow(QLabel("Empower_hardstop_torque_ts"), self.label_Empower_hardstop_torque_ts)
        layout_group_output.addRow(QLabel("Empower_hardstop_torque"), self.label_Empower_hardstop_torque)

        layout_group_output.addRow(QLabel("Empower_series_spring_torque_ts"), self.label_Empower_series_spring_torque_ts)
        layout_group_output.addRow(QLabel("Empower_series_spring_torque"), self.label_Empower_series_spring_torque)

        layout_group_output.addRow(QLabel("Empower_ankle_angle_ts"), self.label_Empower_ankle_angle_ts)
        layout_group_output.addRow(QLabel("Empower_ankle_angle"), self.label_Empower_ankle_angle)

        layout_group_output.addRow(QLabel("Empower_ballscrew_pos_counts_ts"), self.label_Empower_ballscrew_pos_counts_ts)
        layout_group_output.addRow(QLabel("Empower_ballscrew_pos_counts"), self.label_Empower_ballscrew_pos_counts)

        self.banana=(QLabel("OUTPUT Current_"))
        layout_group_output.addRow(self.banana)

        # output   
        layout_group_input.addRow(QLabel("OUTPUT Run_level"), self.input_runlevel)
        #layout_group_input.addRow(QLabel("OUTPUT Run_level"), self.input_current)

        self.sl = QSlider(Qt.Horizontal)
        self.sl.setMinimum(-100)
        self.sl.setMaximum(100)
        self.sl.setValue(0)
        self.sl.setTickPosition(QSlider.TicksBelow)
        self.sl.setTickInterval(10)
        self.sl.setSingleStep(1)
		

        self.layout_main.addWidget(group_output)
        self.layout_main.addWidget(group_input)
        self.layout_main.addWidget(self.sl)
        self.sl.valueChanged.connect(self.valuechange)
        
        # -------

        self.show()  # Windows is hidden by default

    
    
    def valuechange(self):

       self.banana.setText("OUTPUT Current: "+ str(float(self.sl.value())/10))
       self.input_current.twincat_send(float(self.sl.value())/10)
      #self.l1.setFont(QFont("Arial",size))
