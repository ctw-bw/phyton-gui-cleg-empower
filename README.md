# TwinPy PLC GUI #

Basic usage of the TwinPy library with a TwinCAT 3 PLC project. 

See TwinPy: https://bitbucket.org/ctw-bw/twinpy

## Install ##

For more details, find the TwinPy ReadMe. The getting-started steps are largely applicable here too. 

1. Clone repo recursively:  
`git clone <url> --recursive`
   * You can also clone regularly and then pull the submodules:  
    `git submodule update --init`
2. Open a terminal inside the `GUI/` directory
3. Create a virtual environment and activate it:  
`python -m venv venv`  
`venv\Scripts\activate.ps1` or `venv\Scripts\activate.bat`
4. Install the requirements and the TwinPy submodule inside the GUI folder:  
`pip install -r requirements.txt`  
5. Now run the GUI:  
`python main.py`
